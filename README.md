# ma_ProcessCreation

This project creates the redis entries from the log files for the [EventProvider](https://gitlab.com/me33551/ma_EventProvider) and [Orchestrator](https://gitlab.com/me33551/ma_Orchestrator).

After installing ruby and the required gems, the root level log file(s) that should be added to the redis database need to be specified in the code.

Afterwards, redis needs to be installed and made availabel via unix socket.

Then the redis database where the logs should be saved can be changed in the code (be careful this will also need to be changed in other projects reading from the database).

Finally, the script can be made executable ("replayLog_redisList.rb" on Linux) and can then be executed with "./replayLog_redisList.rb"