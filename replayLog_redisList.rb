#!/usr/bin/env ruby


require 'logger'
#require 'net/http'
require 'psych'
require 'json'
require 'redis'
require 'pp'


puts 'hello world'

def readAndWrite(pathToFile, redis, logger)

  logger.debug("replaying trace #{pathToFile}")
  puts "starting file #{pathToFile}"

  #filecontent = IO.read(pathToFile, {:encoding => "utf-8"})
  filecontent = IO.read(pathToFile)

  traceuuid=nil
  Psych.load_stream(filecontent) do |object|
    if object.key?('log') then
      logger.debug('log')
      #puts Psych.to_json(object)
      traceuuid=object['log']['trace']['cpee:uuid']
      if traceuuid.nil? then
        traceuuid=object['log']['trace']['cpee:instance']
      end
      #write 'log' to redis
      redis.rpush(traceuuid,object.to_json)
    elsif object.key?('event') then
      logger.debug('event')
      #write 'event' to redis
      redis.rpush(traceuuid,object.to_json)
      if object.dig('event','cpee:lifecycle:transition')=='activity/receiving' && object.dig('event', 'list', 'data_receiver', 0, 'data').is_a?(Hash) && !object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID').nil? && object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-STATE')=='running' then
        puts object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')
        logger.debug("subprocess - going down one level to #{object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')}")
	puts "going to #{object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')}\n"
        readAndWrite("#{File.dirname(pathToFile)}/#{object.dig('event', 'list', 'data_receiver', 0, 'data', 'CPEE-INSTANCE-UUID')}.xes.yaml", redis, logger)
      end
      if object.dig('event','cpee:lifecycle:transition')=='task/instantiation' && object.dig('event', 'data', 'data_receiver').is_a?(Hash) && !object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID').nil? then
        puts object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')
        logger.debug("subprocess - going down one level to #{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}")
	puts "going to #{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}\n"
        readAndWrite("#{File.dirname(pathToFile)}/#{object.dig('event', 'data', 'data_receiver', 'CPEE-INSTANCE-UUID')}.xes.yaml", redis, logger)
      end
    else
      logger.debug('unknown type')
      puts 'unknown type'
    end
  end

  puts "finishing file #{pathToFile}"

end

logger = Logger.new File.new('replayLog.log', 'w')
redis=Redis.new(
  :path => "/tmp/redis.sock",
  :encoding => 'utf-8'
)
#redis.select 14
redis.select 15
#redis.select 13

#root(s) of batch15
readAndWrite('gv12/batch15/f47bb3a9-b606-475e-8b43-ee981065dcb5.xes.yaml', redis, logger)
readAndWrite('gv12/batch15/679414e0-ab25-430c-8016-11ed04f28bb2.xes.yaml', redis, logger)
